-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for aap
CREATE DATABASE IF NOT EXISTS `aap` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `aap`;


-- Dumping structure for table aap.company_pic
CREATE TABLE IF NOT EXISTS `company_pic` (
  `id_pic` int(11) NOT NULL AUTO_INCREMENT,
  `name_pic` varchar(50) DEFAULT NULL,
  `email_pic` varchar(30) DEFAULT NULL,
  `phone_pic` varchar(13) DEFAULT NULL,
  `address_pic` text,
  `username` varchar(10) DEFAULT NULL,
  `password` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_pic`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table aap.company_pic: ~4 rows (approximately)
DELETE FROM `company_pic`;
/*!40000 ALTER TABLE `company_pic` DISABLE KEYS */;
INSERT INTO `company_pic` (`id_pic`, `name_pic`, `email_pic`, `phone_pic`, `address_pic`, `username`, `password`) VALUES
	(1, 'Yudi Widiawan', 'makanjukut@gmail.com', '087718938582', 'Jalan Pasir Koja', 'yudi', 'yudi'),
	(9, 'Debi Dwitama Yusup', 'dwittamma48@gmail.com', '087718938581', 'Antapani', 'dwittamma', 'ce60e43f0b'),
	(10, 'Debi', 'debi@gmail.com', '087718938581', NULL, NULL, NULL);
/*!40000 ALTER TABLE `company_pic` ENABLE KEYS */;


-- Dumping structure for table aap.company_profile
CREATE TABLE IF NOT EXISTS `company_profile` (
  `company_name` varchar(50) DEFAULT NULL,
  `company_email` varchar(30) DEFAULT NULL,
  `company_address` text,
  `company_city` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table aap.company_profile: ~0 rows (approximately)
DELETE FROM `company_profile`;
/*!40000 ALTER TABLE `company_profile` DISABLE KEYS */;
INSERT INTO `company_profile` (`company_name`, `company_email`, `company_address`, `company_city`) VALUES
	('mahira', 'mahira@gmail.com', 'dago', 'BANDUNG'),
	('PT Mahira', 'ptmahira@gmail.com', 'Dago', 'Bandung');
/*!40000 ALTER TABLE `company_profile` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
